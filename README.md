# dongle-bebis

> A banky idea project

Coming soon at a banksy perpis near you...

## Maintainer

- Andrew Zyabin - @schas002 - [@zyabin101@botsin.space](https://botsin.space/@zyabin101)

## Contribute

:clap: Every issue and PR is welcome! :clap:

Every contributor to this repository must follow the code of conduct, which is: don't be rude.

## License

[MIT](LICENSE) &copy; Andrew Zyabin
